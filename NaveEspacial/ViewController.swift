//
//  ViewController.swift
//  NaveEspacial
//
//  Created by Miguel Ángel Jareño Escudero on 30/10/16.
//  Copyright © 2016 Miguel Ángel Jareño Escudero. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    
    @IBOutlet weak var emergency_bt: UIButton!
    @IBOutlet weak var sw_value: UITextView!
    @IBOutlet weak var sl_value: UILabel!
    
    @IBAction func slider(_ sender: UISlider) {
        
        sl_value.text = String(Int(sender.value))
    }
    
    
    @IBAction func activationSw(_ sender: UISwitch) {
        if(sender.isOn){
            sw_value.text = "Control emergencia \n activo"
            emergency_bt.isEnabled = true
            emergency_bt.alpha = 1.0
            print("Enabled")
        }else{
            sw_value.text = "Control emergencia \n desactivado"
            emergency_bt.isEnabled = false
            emergency_bt.alpha = 0.5
            print("Disabled")
        }
    }

    @IBAction func emergency(_ sender: UIButton) {
        
        let emergencySheet = UIAlertController(title: "¡Emergencia!", message: "Selecciona que hacer",preferredStyle: .actionSheet)
        let naveSalvavidas = UIAlertAction(title: "Nave Salvavidas", style: .default){
            action in print("nave salvavidas activado")
        }
        let hiperespacio = UIAlertAction(title: "Hiperespacio", style: .default){
            action in print("hiperespacio activado")
        }
        let autodestruccion = UIAlertAction(title: "Autodestrucción", style: .destructive){
            action in print("autodestrucción activada")
        }
        emergencySheet.addAction(naveSalvavidas)
        emergencySheet.addAction(hiperespacio)
        emergencySheet.addAction(autodestruccion)
        self.present(emergencySheet, animated: true)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

